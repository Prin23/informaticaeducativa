@extends('layouts.app')

@section('content')
        <div class="row">
            <div class="col-sm-12">
                <h1 class="pull-left">Edit Clase</h1>
            </div>
        </div>

        @include('core-templates::common.errors')

        <div class="row">
            {!! Form::model($clase, ['route' => ['clases.update', $clase->id], 'method' => 'patch']) !!}

            @include('clases.fields')

            {!! Form::close() !!}
        </div>
@endsection

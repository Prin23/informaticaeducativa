@extends('layouts.app')

@section('content')
    @include('clases.show_fields')

    <div class="form-group">
           <a href="{!! route('clases.index') !!}" class="btn btn-default">Back</a>
    </div>
@endsection

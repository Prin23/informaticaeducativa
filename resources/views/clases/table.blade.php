<table class="table table-responsive" id="clases-table">
    <thead>
        <th>Titulo</th>
        <th>Contenido</th>
        <th>Modulo Id</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($clases as $clase)
        <tr>
            <td>{!! $clase->titulo !!}</td>
            <td>{!! $clase->contenido !!}</td>
            <td>{!! $clase->modulo_id !!}</td>
            <td>
                {!! Form::open(['route' => ['clases.destroy', $clase->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('clases.show', [$clase->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('clases.edit', [$clase->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

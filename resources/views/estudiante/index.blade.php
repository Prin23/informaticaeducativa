@extends('layouts.app')
@section('title','Mooc Ingles Tecnico | Inicio')
@section('content')
	<div class="row">
	@include('layouts.elements.nav')
	</div>
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default panel-body">    
		        <h1>Mooc de Ingles Tecnico</h1>
		        <br>
		        <p class="text-justify">
		        	Descripcion del curso: Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
		        </p>
		    </div> 
		 </div>
	</div>
	<div class="row">
		<div class="col-md-10 col-md-offset-1">	
		<h1>Modulos</h1>
		</div>
		<div class="col-md-5 col-md-offset-1">	
			@foreach ($modulos as $modulo) 
			<div class="panel panel-default panel-body">   	     
		        <br>
		        <p class="text-justify">
		        	<h4 class="text-center"> {{ $modulo->titulo }} </h4>
		        	{{ $modulo->descripcion }}
		        </p>
		        	@if($modulo->id == "1") 
		        	Pasado Simple. <a href="{{ route('estudiante.modulos.1.clase1')}}"> Clase </a> | Practica: 0 pts.
		        	<br>
		        	<br>
		        	Presente Simple. <a href="#"> Clase </a> | Practica: 0 pts.
		        	<br>
		        	<br>
		        	Futuro Simple. <a href="#"> Clase </a> | Practica: 0 pts.
		        	<br>
		        	@elseif($modulo->id == "2")
		        	Pasado Continuo. <a href="#"> Clase </a> | Practica: 0 pts.
		        	<br>
		        	<br>
		        	Presente Presente y Futuro Continuo. <a href="#"> Clase </a> | Practica: 0 pts.
		        	<br>
		        	<br>
		        	Tiempos Perfectos. <a href="#"> Clase </a> | Practica: 0 pts.
		        	<br>
		        	@endif
		        </p>
		    </div>
			@endforeach	 
		</div>
	</div>
	<div class="row">	
		<div class="col-md-10 col-md-offset-1"> 
		<h1>Mi Certificado</h1>
		</div>
		<div class="col-md-10 col-md-offset-1">	
			<div class="panel panel-default panel-body">   	        
		        <br>
		        <p class="text-justify">
		        	Descripcion del curso: Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
		        </p>
		    </div> 
		</div>
	</div> 
@endsection


@extends('layouts.app')
@section('title','Mooc Ingles Tecnico | Practica 1')
@section('content')
	<div class="row">
	@include('layouts.elements.nav')
	</div>
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default panel-body">    
		        <h1>{{$evaluacion->titulo}}</h1>
		        <br>
		        <p class="text-justify">
		        	Consigue una puntuacion de 50 pts o mas para avanzar en el curso!.
		        </p>
		 	</div>
		</div>
		<div class="row">
			<div class="col-md-10 col-md-offset-1">
				<div class="panel panel-default panel-body">    
			        <h1>Lee cuidadosamente y responde:</h1>
			        <br>
			        {!! Form::open(['route' => 'public.store']) !!}

			     	@foreach ($preguntas as $pregunta)
			     		<p>
			        		<h4>-{{ $pregunta->pregunta }}</h4>
			        	</p>
			        	<hr>
			        	@foreach($pregunta->opciones as $opcion)
    					{!! Form::label('opcion'.$pregunta->id, $opcion->opcion) !!}
    					<br>
    					<p>{!! Form::radio('opcion'.$pregunta->id, $opcion->id, ['class' => 'form-control']) !!}
    					</p>
    					<br>
			        	@endforeach	
			     	@endforeach 
			     	<!-- Submit Field -->
					<div class="form-group col-sm-12">
    				<center>
    				{!! Form::submit('Termine!', ['class' => 'btn btn-primary']) !!}
    				</center>
			    {!! Form::close() !!}
			    </div> 
			 </div>
		</div>
	</div> 
@endsection


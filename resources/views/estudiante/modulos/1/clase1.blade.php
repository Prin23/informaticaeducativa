@extends('layouts.app')
@section('title','Mooc Ingles Tecnico | Clase 1')
@section('content')
	<div class="row">
	@include('layouts.elements.nav')
	</div>
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default panel-body">    
		        <h1>Clase 1: Pasado Simple.</h1>
		        <br>
		        <p class="text-justify">
		        	El pasado simple se suele utilizar para referirnos a acciones que tuvieron lugar en un momento concreto del pasado. En este caso se utilizan partículas como yesterday (ayer) o last year (el pasado año).
		        </p>
		        <p>
		        	<strong>Ejemplo: She finished school last year ➜ Acabó el colegio el pasado año.</strong>
		        	</p>
		        <p>
		        	También se utiliza el pasado para acciones que ocurrieron en el pasado y que han finalizado, aunque no se mencione el momento preciso.
		        </p>
		        <p>
		        	<strong>Ejemplo: Who wrote that letter? ➜ ¿Quién escribió esta carta?. </strong>
		        </p>
		    </div> 
		 </div>
	</div>
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default panel-body">    
		        <h1>Estructura:</h1>
		        <br>
		        <p class="text-justify">
		        	La estructura del "pasado simple" en inglés es:
		        </p>
		        <p>
		        	<strong>[SUJETO] + [VERBO EN PASADO, TERMINACIÓN "-ed"].</strong>
		        </p>
		        <p>
		        	Como norma general, para formar el pasado en inglés se añade "-ed" a un verbo.
		        </p>
		        <p>
		        	<strong> Ejemplo: work➜worked. </strong>
		        </p>
		    </div> 
		 </div>
	</div>
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default panel-body">    
		        <h1>Casos especiales:</h1>
		        <br>
		        <p class="text-justify">
		        	Para los verbos irregulares hay que memorizar su forma de pasado.
		        </p>
		        <p>
		        	<strong>Ejemplo: I went to the University of Oxford ➜ Fui a la universidad de Oxford.</strong>
		        </p>
		        <p>
		        	Para formar el pasado se sustituye la "-e" final por "-ed".
		        </p>
		        <p>
		        	<strong>Ejemplo: I Use ➜ i used.</strong>
		        </p>
		        <p>
		        	La formación de la negación en "past simple" es más sencilla que la afirmación. Su estructura es:
		        </p>
		        <p>
		        	<strong> [SUJETO] + did + not + [VERBO EN INFINITIVO (sin to)]. </strong>
		        </p>
		        <p>
		        	<strong>Ejemplo: I did not sing ➜ yo no canté.</strong>
		        </p>
		    </div> 
		 </div>
		 <div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default panel-body">    
		        <h1>Formas Interrogativas:</h1>
		        <br>
		        <p class="text-justify">
		        	Para construir la interrogación se utiliza el verbo auxiliar to do en pasado (did):
		        </p>
		        <p>
		        	<strong>Did + [SUJETO] + [VERBO EN INFINITIVO (sin to)] ?.
		        	</strong>
		        </p>
		        <p>
		        	<strong> Ejemplo: did I sing? ➜ ¿canté?.
		        	</strong>
		        </p>
		        <p>
		        	Si la interrogación tuviera una partícula interrogativa la estructura sería:
		        </p>
		        <p>	
		        <strong> [PARTÍCULA INTERROGATIVA] + did + [SUJETO] + [VERBO EN INFINITIVO]?.
		        </strong>
		        </p>
		        <p>
		        <strong> Ejemplo: What did you sing? ➜ ¿Qué cantaste?.
		        </strong>
		        </p>
		    </div>
		    <center><a class="btn btn-primary" href="{{ route('estudiante.modulos.1.practica1')}}">Ir a la practica!</a></center>
		 </div>
	</div>
	</div> 
@endsection


<div class="navbar navbar-inverse navbar-fixed-top">
    <div class="conten-nav">
        <div class="navbar-header">
            <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        	<div class="col-md-10 col-md-offset-1">
		        <div class="collapse navbar-collapse" id="navbar-main">
		        	<ul class="nav navbar-nav">	 
			            <li><a href="#"> <img class="img img-responsive img-circle avatar" src="{{ asset($persona->avatar) }} "> </a></li>
			        </ul>    
		            <ul class="nav navbar-nav">
		                <li><a class="navbar-brand" href="/">  {!! $persona->nombre.' '.$persona->apellido.' '.$persona->cedula !!} </a></li>
		            </ul>
		            @if(Auth::user())
			            <ul class="nav navbar-nav pull-right">	 
			                <li><a href="#"><span class="glyphicon glyphicon-home"></span>  Modulos </a></li>
			               	<li><a href="#"><span class="glyphicon glyphicon-home"></span>  Mi Certiificado </a></li>
			                <li><a href="#"><span class="glyphicon glyphicon-home"></span>  Descargas </a></li>
			                <li><a href="{{ route('forum.home') }}"><span class="glyphicon glyphicon-home"></span>  Foro </a></li>

			                <li class="dropdown">
	            				<a href="#" data-toggle="dropdown" class="dropdown-toggle"><span class="glyphicon glyphicon-user"></span> <b class="caret"></b></a>
								<ul class="dropdown-menu">
									<li><a href="#"><i class="glyphicon glyphicon-user"></i> Mi usuario</a></li>
									<li><a href="{{ route('auth.logout') }}"><i class="glyphicon glyphicon-off"></i> Cerrar Sesion</a></li>		 
	        					</ul>
	        				</li>
		           		</ul>
			        @endif     
		        </div>
		    </div>    
    	</div>
	</div>
</div>	

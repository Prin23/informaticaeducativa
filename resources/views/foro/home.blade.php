@extends('layouts.app')

@section('title', 'Foro')

@section('css')
	<link rel="stylesheet" type="text/css" href="{{ asset('css/estilos.css') }}">
@endsection

@section('content')
	<br>
	<div class="row">
		<div class="col-xs-12">
			@foreach($secciones as $seccion)
				<div class="row">
					<div class="col-md-12">
						<div class="titulo-sec">
							{{ $seccion->titulo }}
						</div>
						@foreach($seccion->temas as $tema)
							<div class="tema-foro">
								<img src="{{ asset($tema->miniatura) }}" class="img-circle">
								<h4><a href="{{ route('forum.tema', $tema->slug) }}">{{ $tema->titulo }}</a></h4>
								<p class="small">{{ $tema->descripcion }}</p>
							</div>
						@endforeach
					</div>
				</div>
			@endforeach
		</div>
	</div>
@endsection
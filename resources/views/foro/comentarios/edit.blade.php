@extends('layouts.app')

@section('content')
        <div class="row">
            <div class="col-sm-12">
                <h1 class="pull-left">Edit Comentario</h1>
            </div>
        </div>

        @include('core-templates::common.errors')

        <div class="row">
            {!! Form::model($comentario, ['route' => ['comentarios.update', $comentario->id], 'method' => 'patch']) !!}

            @include('comentarios.fields')

            {!! Form::close() !!}
        </div>
@endsection

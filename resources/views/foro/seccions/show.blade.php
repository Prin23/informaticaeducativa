@extends('layouts.app')

@section('content')
    @include('seccions.show_fields')

    <div class="form-group">
           <a href="{!! route('seccions.index') !!}" class="btn btn-default">Back</a>
    </div>
@endsection

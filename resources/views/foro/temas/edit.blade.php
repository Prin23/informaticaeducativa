@extends('layouts.app')

@section('content')
        <div class="row">
            <div class="col-sm-12">
                <h1 class="pull-left">Edit Tema</h1>
            </div>
        </div>

        @include('core-templates::common.errors')

        <div class="row">
            {!! Form::model($tema, ['route' => ['temas.update', $tema->id], 'method' => 'patch']) !!}

            @include('temas.fields')

            {!! Form::close() !!}
        </div>
@endsection

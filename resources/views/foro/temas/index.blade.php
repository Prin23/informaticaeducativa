@extends('layouts.app')

@section('title')
	{{ $tema->titulo }}
@endsection

@section('css')
	<link rel="stylesheet" type="text/css" href="{{ asset('plugins/dist/ui/trumbowyg.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/estilos.css') }}">
@endsection

@section('content')
	<br>
    <div class="row">
      	<div class="col-xs-12">
      		<div class="row">
      			<div class="col-md-12">
      				<div class="titulo-sec">
	      				Posts <a href="#" class="pull-right btn btn-success btn-sm" title="Nuevo Post" data-toggle="modal" data-target="#nuevoPost"><i class="glyphicon glyphicon-plus"></i></a>
	      				<a href="{{ route('forum.home') }}" class="pull-right btn btn-primary btn-sm" title="Regresar"><i class="glyphicon glyphicon-chevron-left"></i></a>
	      			</div>
	      			@foreach($tema->posts as $post)
	      				<div class="tema-foro">
							<h4><a href="{{ route('posts.show', $post->slug) }}">{{ $post->titulo }}</a></h4>
							<p class="small">{{ $post->descripcion }}</p>
						</div>
	      			@endforeach
      			</div>
      		</div>
      	</div>
    </div>
<div class="modal fade" id="nuevoPost" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Nuevo Post</h4>
			</div>
			<div class="modal-body">
				{!! Form::open(['route' => 'posts.store', 'method' => 'POST']) !!}
					<div class="form-group">
						{!! Form::label('titulo', 'Titulo:') !!}
						{!! Form::text('titulo', null, ['class' => 'form-control', 'placeholder' => 'Ingrese el Titulo', 'maxlength' => '60', 'required']) !!}
					</div>
					<div class="form-group">
						{!! Form::label('descripcion', 'Descripción:') !!}
						{!! Form::text('descripcion', null, ['class' => 'form-control', 'placeholder' => 'breve descripcion', 'maxlength' => '120', 'required']) !!}
					</div>
					<div class="form-group">
						{!! Form::label('post', 'Tu Post:') !!}
						{!! Form::textarea('post', null, ['class' => 'form-control cuerpotxt', 'placeholder' => 'escribe tu post', 'required']) !!}
					</div>
					<div class="form-group">
						{!! Form::hidden('tema_id', $tema->id) !!}
						<button type="button" class="btn btn-warning" data-dismiss="modal">Cerrar</button>
						<button type="submit" class="btn btn-primary">Registrar</button>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
@endsection
@section('scripts')
	<script src="{{ asset('plugins/dist/trumbowyg.js') }}"></script>
	<script src="{{ asset('plugins/dist/langs/es.min.js') }}"></script>
	<script type="text/javascript">
		$('.cuerpotxt').trumbowyg({
			lang : 'es',
			btns: ['viewHTML', 'formatting', 'btnGrp-design', 'btnGrp-justify', 'btnGrp-lists', 'removeformat', 'link', 'fullscreen'],
		});
	</script>
@endsection

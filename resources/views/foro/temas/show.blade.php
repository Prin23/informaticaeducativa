@extends('layouts.app')

@section('content')
    @include('temas.show_fields')

    <div class="form-group">
           <a href="{!! route('temas.index') !!}" class="btn btn-default">Back</a>
    </div>
@endsection

@extends('layouts.app')

@section('title')
	{{ $post->titulo }}
@endsection

@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/dist/ui/trumbowyg.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/estilos.css') }}">
@endsection

@section('content')
    <br>
    <div class="row">
    	<div class="col-xs-12">
            <div class="row">
                <div class="col-xs-12">
                    <a href="{{ route('forum.tema', $post->tema->slug) }}" class="pull-right btn btn-primary btn-sm" title="Regresar"><i class="glyphicon glyphicon-chevron-left"></i></a>
                </div>
                <br>
            </div>
            <hr>
    		<div class="row post">
            <br>
                <div class="col-xs-2" style="border-right: 0.5px solid silver;">
                    @if($post->user->persona == null)
                        <h4>Admin Test</h4>
                    @else
                        <h4>{{ $post->user->persona->nombre }} {{ $post->user->persona->apellido }}</h4>
                    @endif
                    <p class="small">Creado el: {{ $post->created_at }}</p>
                </div>
                <div class="col-xs-10 cuerpost">
                    <p class="text-justify">
                        {{ $post->post }}
                    </p>
                </div>
    		</div>
    	</div>
    </div>
    <hr>
    @foreach($post->comentarios as $comentario)
    <div class="row comentario">
        <div class="col-xs-12">
            <div class="row">
                <div class="col-xs-2" style="border-right: 0.5px solid silver;">
                    @if($comentario->user->persona == null)
                        <h4>Admin Test</h4>
                    @else
                        <img src="{{ asset($comentario->user->persona->avatar) }}" class="img-circle img-responsive">
                        <h4>{{ $comentario->user->persona->nombre.' '.$comentario->user->persona->apellido }}</h4>
                    @endif
                    <p class="small">Creado el: {{ $comentario->created_at }}</p>
                </div>
                <div class="col-xs-10 cuerpost">
                    <p class="text-justify">
                        {!! $comentario->comentario !!}
                    </p>
                </div>
            </div>
        </div>
    </div>
    <hr>
    @endforeach
    <div class="row">
        <div class="col-xs-12">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    {!! Form::open(['route' => 'comentarios.store', 'method' => 'POST']) !!}
                        {!! Form::hidden('post_id', $post->id) !!}
                        <div class="form-group">
                            {!! Form::label('comentario', 'Deja tu comentario:') !!}
                            {!! Form::textarea('comentario', null, ['class' => 'form-control cuerpotxt', 'placeholder' => 'Escribe tu comentario...', 'required']) !!}
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-success btn-sm">Enviar</button>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('plugins/dist/trumbowyg.js') }}"></script>
    <script src="{{ asset('plugins/dist/langs/es.min.js') }}"></script>
    <script type="text/javascript">
        $('.cuerpotxt').trumbowyg({
            lang : 'es',
            btns: ['viewHTML', 'formatting', 'btnGrp-design', 'btnGrp-justify', 'btnGrp-lists', 'removeformat', 'link', 'fullscreen'],
        });
    </script>
@endsection
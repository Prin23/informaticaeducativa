@extends('layouts.app')
@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/jqueryui/jquery-ui.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/jqueryui/jquery-ui.structure.css') }}">
@endsection
@section('content')
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <h1 class="pull-left">Registrate!</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
        {!! Form::open(['route' => 'public.store', 'files' => true]) !!}
            @include('public.fields')
        {!! Form::close() !!}
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('plugins/jqueryui/jquery-ui.js') }}"></script>
    <script type="text/javascript">
          $(function() {
            $( "#fecha" ).datepicker({
                dateFormat: "yy-mm-dd",
            });
          });
    </script>
@endsection

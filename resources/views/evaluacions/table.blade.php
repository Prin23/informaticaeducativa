<table class="table table-responsive" id="evaluacions-table">
    <thead>
        <th>Titulo</th>
        <th>Tipo</th>
        <th>Modulo Id</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($evaluacions as $evaluacion)
        <tr>
            <td>{!! $evaluacion->titulo !!}</td>
            <td>{!! $evaluacion->tipo !!}</td>
            <td>{!! $evaluacion->modulo_id !!}</td>
            <td>
                {!! Form::open(['route' => ['evaluacions.destroy', $evaluacion->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('evaluacions.show', [$evaluacion->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('evaluacions.edit', [$evaluacion->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

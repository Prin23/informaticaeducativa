@extends('layouts.app')

@section('content')
        <h1 class="pull-left">Evaluacions</h1>
        <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('evaluacions.create') !!}">Add New</a>

        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>

        @include('evaluacions.table')
        
@endsection

@extends('layouts.app')

@section('content')
        <div class="row">
            <div class="col-sm-12">
                <h1 class="pull-left">Edit Modulo</h1>
            </div>
        </div>

        @include('core-templates::common.errors')

        <div class="row">
            {!! Form::model($modulo, ['route' => ['modulos.update', $modulo->id], 'method' => 'patch']) !!}

            @include('modulos.fields')

            {!! Form::close() !!}
        </div>
@endsection

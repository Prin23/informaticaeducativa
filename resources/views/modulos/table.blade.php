<table class="table table-responsive" id="modulos-table">
    <thead>
        <th>Titulo</th>
        <th>Descripcion</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($modulos as $modulo)
        <tr>
            <td>{!! $modulo->titulo !!}</td>
            <td>{!! $modulo->descripcion !!}</td>
            <td>
                {!! Form::open(['route' => ['modulos.destroy', $modulo->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('modulos.show', [$modulo->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('modulos.edit', [$modulo->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

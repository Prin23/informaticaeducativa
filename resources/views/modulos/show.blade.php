@extends('layouts.app')

@section('content')
    @include('modulos.show_fields')

    <div class="form-group">
           <a href="{!! route('modulos.index') !!}" class="btn btn-default">Back</a>
    </div>
@endsection

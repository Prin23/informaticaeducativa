@extends('layouts.app')	
@section('content')		
	<div class="row">
		<div class="col-md-12">
			<h2 class="text-center">¡Welcome! Student</h2>
			<h5 class="text-center">Cuidado con Wel.... Come estudiantes...</h5>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4 col-md-offset-4">
			{!! Form::open(['route' => 'auth.login', 'method' => 'POST', 'class' => 'form']) !!}
				<div class="form-group">
					{!! Form::label('email', 'E-Mail:') !!}
					{!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Ingrese su E-Mail', 'required']) !!}
				</div>
				<div class="form-group">
					{!! Form::label('password', 'Contraseña:') !!}
					{!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Ingrese su Contraseña', 'required']) !!}
				</div>
				<button type="submit" class="btn btn-primary">Entrar</button>
			{!! Form::close() !!}
			<hr>
			<a href="{!! route('public.registrar') !!}" class="small">Nuevo Usuario</a>
		</div>
	</div>
@endsection


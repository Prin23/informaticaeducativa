<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $opcion->id !!}</p>
</div>

<!-- Opcion Field -->
<div class="form-group">
    {!! Form::label('opcion', 'Opcion:') !!}
    <p>{!! $opcion->opcion !!}</p>
</div>

<!-- Pregunta Id Field -->
<div class="form-group">
    {!! Form::label('pregunta_id', 'Pregunta Id:') !!}
    <p>{!! $opcion->pregunta_id !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $opcion->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $opcion->updated_at !!}</p>
</div>


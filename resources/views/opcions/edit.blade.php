@extends('layouts.app')

@section('content')
        <div class="row">
            <div class="col-sm-12">
                <h1 class="pull-left">Edit Opcion</h1>
            </div>
        </div>

        @include('core-templates::common.errors')

        <div class="row">
            {!! Form::model($opcion, ['route' => ['opcions.update', $opcion->id], 'method' => 'patch']) !!}

            @include('opcions.fields')

            {!! Form::close() !!}
        </div>
@endsection

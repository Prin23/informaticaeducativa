@extends('layouts.app')

@section('content')
    @include('opcions.show_fields')

    <div class="form-group">
           <a href="{!! route('opcions.index') !!}" class="btn btn-default">Back</a>
    </div>
@endsection

<table class="table table-responsive" id="opcions-table">
    <thead>
        <th>Opcion</th>
        <th>Pregunta Id</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($opcions as $opcion)
        <tr>
            <td>{!! $opcion->opcion !!}</td>
            <td>{!! $opcion->pregunta_id !!}</td>
            <td>
                {!! Form::open(['route' => ['opcions.destroy', $opcion->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('opcions.show', [$opcion->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('opcions.edit', [$opcion->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

<!-- Opcion Field -->
<div class="form-group col-sm-6">
    {!! Form::label('opcion', 'Opcion:') !!}
    {!! Form::text('opcion', null, ['class' => 'form-control']) !!}
</div>

<!-- Pregunta Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('pregunta_id', 'Pregunta Id:') !!}
    {!! Form::number('pregunta_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('opcions.index') !!}" class="btn btn-default">Cancel</a>
</div>

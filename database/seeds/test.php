<?php

use Illuminate\Database\Seeder;

class test extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
        	'email' 	=> 'test@example.com',
        	'password' 	=> bcrypt('test'),
        	'tipo'		=> 'admin',
        ]);

        DB::table('users')->insert([
            'email'     => 'nixon@example.com',
            'password'  => bcrypt('nixon'),
            'tipo'      => 'estudiante',
        ]);

        DB::table('personas')->insert([
            'nombre'     => 'Nixon',
            'apellido'  => 'Torres',
            'cedula'      => '12345678',
            'fech_nac' => '1994-09-26',
            'avatar' => '/img/avatar/Nixon_12345678.jpg',
            'user_id' => '2'

        ]);

        DB::table('modulos')->insert([
            'titulo'     => 'Modulo 1 (Basico): Tiempos Verbales Simples.',
            'descripcion'  => 'Los tiempos verbales en inglés expresan el momento en que se realiza una acción (verbo). Determinan si la acción ya fue realizada (pasado), se está realizando (presente) o se va a realizar (futuro). El tiempo verbal simple muestra cuándo la acción ocurre.'
        ]);

        DB::table('clases')->insert([
            'titulo'     => 'Clase 1: Pasado Simple.',
            'contenido'  => 'Prueba',
            'modulo_id' => '1'
        ]);
        DB::table('clases')->insert([
            'titulo'     => 'Clase 2: Presente Simple.',
            'contenido'  => 'Prueba',
            'modulo_id' => '1'
        ]);
        DB::table('clases')->insert([
            'titulo'     => 'Clase 3: Futuro Simple.',
            'contenido'  => 'Prueba',
            'modulo_id' => '1'
        ]);

        DB::table('evaluacions')->insert([
            'titulo'     => 'Practica 1: Pasado Simple.',
            'tipo'  => 'practica',
            'modulo_id' => '1'
        ]);

        DB::table('preguntas')->insert([
            'pregunta'     => '¿En que ocaciones se utiliza en pasado simple?',
            'evaluacion_id'  => '1'
        ]);

        DB::table('opcions')->insert([
            'opcion'     => 'Para referirnos a acciones que tuvieron lugar en un momento concreto del pasado.',
            'tipo' => 'verdadera',
            'pregunta_id'  => '1'
        ]);

        DB::table('opcions')->insert([
            'opcion'     => 'Para referirnos a acciones no han ocurrido aun, pero sabemos que ocurruran.',
            'tipo' => 'falsa',
            'pregunta_id'  => '1'
        ]);

        DB::table('opcions')->insert([
            'opcion'     => 'Para referirnos a acciones del pasado que aun continuamos haciendo.',
            'tipo' => 'falsa',
            'pregunta_id'  => '1'
        ]);

        DB::table('preguntas')->insert([
            'pregunta'     => '¿Cual de estas horaciones esta escrita correctamente?',
            'evaluacion_id'  => '1'
        ]);

        DB::table('opcions')->insert([
            'opcion'     => 'Who writed that letter',
            'tipo' => 'falsa',
            'pregunta_id'  => '2'
        ]);

        DB::table('opcions')->insert([
            'opcion'     => 'Who wrote that letter?',
            'tipo' => 'verdadera',
            'pregunta_id'  => '2'
        ]);

        DB::table('opcions')->insert([
            'opcion'     => 'Who did write that letter?',
            'tipo' => 'falsa',
            'pregunta_id'  => '2'
        ]);

        DB::table('preguntas')->insert([
            'pregunta'     => '¿Cual es la estructura de la forma negada del pasado simple?',
            'evaluacion_id'  => '1'
        ]);

        DB::table('opcions')->insert([
            'opcion'     => '[SUJETO] + not + [VERBO EN INFINITIVO (sin to)].',
            'tipo' => 'falsa',
            'pregunta_id'  => '3'
        ]);

        DB::table('opcions')->insert([
            'opcion'     => '[SUJETO] + did + not + [VERBO EN INFINITIVO (sin to)].',
            'tipo' => 'verdadera',
            'pregunta_id'  => '3'
        ]);

        DB::table('opcions')->insert([
            'opcion'     => '[SUJETO] + not + [VERBO GERUNDIO].',
            'tipo' => 'falsa',
            'pregunta_id'  => '3'
        ]);

        DB::table('preguntas')->insert([
            'pregunta'     => 'What did you sing? ¿Como se traduce correctamente?',
            'evaluacion_id'  => '1'
        ]);

        DB::table('opcions')->insert([
            'opcion'     => '¿Qué cantaste?',
            'tipo' => 'falsa',
            'pregunta_id'  => '4'
        ]);

        DB::table('opcions')->insert([
            'opcion'     => '¿Qué cante?',
            'tipo' => 'verdadera',
            'pregunta_id'  => '4'
        ]);

        DB::table('opcions')->insert([
            'opcion'     => '¿Qué Cantare?',
            'tipo' => 'falsa',
            'pregunta_id'  => '4'
        ]);

        DB::table('preguntas')->insert([
            'pregunta'     => '¿I did not sing? ¿Esta escrito correctamente?',
            'evaluacion_id'  => '1'
        ]);


        DB::table('opcions')->insert([
            'opcion'     => 'No.',
            'tipo' => 'falsa',
            'pregunta_id'  => '5'
        ]);

        DB::table('opcions')->insert([
            'opcion'     => 'Si.',
            'tipo' => 'verdadera',
            'pregunta_id'  => '5'
        ]);

        DB::table('evaluacions')->insert([
            'titulo' => 'Practica 2: Presente Simple.',
            'tipo'  => 'practica',
            'modulo_id' => '1'
        ]);
        DB::table('evaluacions')->insert([
            'titulo' => 'Prueba: Tiempos Verbales Simples.',
            'tipo'  => 'prueba',
            'modulo_id' => '1'
        ]);

        DB::table('modulos')->insert([
            'titulo'     => 'Modulo 2 (Intermedio): Tiempos verbales Continuos y Perfectos.',
            'descripcion'  => 'Los tiempos continuos o progresivos Indican que la acción empezó, continuó y fue completada, mientras que los perfectos indican  acciones que suceden en un pasado reciente y que guardan alguna relación con el presente.'
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class testforo extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
    	$faker = Faker::create();
    	$i = 1;

    	for ($i=1; $i < 6 ; $i++) { 
    		DB::table('seccions')->insert([
    			'titulo'	=> $faker->unique()->text($maxNbChars = 60),
    			'user_id'	=> '1',
	        ]);
            $y = 1;

            for ($y=1; $y < 6 ; $y++) { 
                DB::table('temas')->insert([
                    'titulo'        => $faker->unique()->text($maxNbChars = 60),
                    'slug'          => $faker->unique()->slug,
                    'descripcion'   => $faker->text($maxNbChars = 120),
                    'miniatura'     => 'foro/miniaturas/test.jpg',
                    'seccion_id'    => $i,
                    'user_id'       => '1',
                ]);
            }
    	}
    }
}

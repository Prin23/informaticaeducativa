<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateopcionsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('opcions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('opcion');
            $table->enum('tipo',['verdadera','falsa'])->default('falsa');
            $table->integer('pregunta_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('pregunta_id')->references('id')->on('preguntas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('opcions');
    }
}

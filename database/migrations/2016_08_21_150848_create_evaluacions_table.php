<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateevaluacionsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('evaluacions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('titulo');
            $table->enum('tipo', ['prueba','practica'])->default('prueba');
            $table->integer('modulo_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('modulo_id')->references('id')->on('modulos');
        });

        Schema::create('persona_evaluacion', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('notas');
            $table->integer('intentos');
            $table->integer('persona_id')->unsigned();
            $table->integer('evaluacion_id')->unsigned();
            $table->timestamps();

            $table->foreign('persona_id')->references('id')->on('personas');
            $table->foreign('evaluacion_id')->references('id')->on('evaluacions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('personas');
        Schema::drop('evaluacions');
    }
}

<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Registrarte
Route::get('/public/registrar', [
    'uses'  => 'PersonaController@registrar',
    'as'    => 'public.registrar'
]);
Route::post('/public/store', [
    'uses'  => 'PersonaController@store',
    'as'    => 'public.store'
]);

Route::group(['middleware' => 'auth'], function () {
	
    //Modulos del uduario tipo:estudiante
        Route::get('estudiante/', [
        'uses'  => 'PersonaController@inicio',
        'as'    => 'estudiante.inicio'
        ]);

        Route::get('/estudiante/modulos/1/clase1', [
        'uses'  => 'ClaseController@clase1',
        'as'    => 'estudiante.modulos.1.clase1'
        ]);
        Route::get('/estudiante/modulos/1/practica1', [
        'uses'  => 'EvaluacionController@practica1',
        'as'    => 'estudiante.modulos.1.practica1'
        ]);


    Route::resource('clases', 'ClaseController');

    Route::resource('personas', 'PersonaController');

    Route::resource('modulos', 'ModuloController');

    Route::resource('videos', 'VideoController');

    Route::resource('imagens', 'ImagenController');

    Route::resource('opcions', 'OpcionController');

    Route::resource('seccions', 'SeccionController');

    Route::resource('temas', 'TemaController');

    Route::resource('posts', 'PostController');

    Route::resource('comentarios', 'ComentarioController');

    Route::resource('evaluacions', 'EvaluacionController');
});

//rutas temporales del foro
Route::controller('forum', 'foroController', [
    'getIndex'      => 'forum.home',
    'getIndexTema'  => 'forum.tema',  
]);

Route::get('/', [
    'uses'  => 'Auth\AuthController@getLogin',
    'as'    => 'auth.login'
]);

Route::post('/', [
    'uses'  => 'Auth\AuthController@postLogin',
    'as'    => 'auth.login'
]);

Route::get('/logout', [
    'uses'  => 'Auth\AuthController@getLogout',
    'as'    => 'auth.logout'
]);
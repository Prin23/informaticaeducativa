<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Seccion;
use App\Models\Tema;
use App\Models\Post;

class foroController extends Controller
{
    public function getIndex()
    {
    	$secciones = Seccion::orderBy('created_at', 'ASC')->get();

    	return view('foro.home')->with('secciones', $secciones);
    }

    public function getIndexTema($slug)
    {
    	$tema = Tema::where('slug', $slug)->firstOrFail();

    	return view('foro.temas.index')->with('tema', $tema);
    }
}

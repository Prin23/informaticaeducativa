<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateClaseAPIRequest;
use App\Http\Requests\API\UpdateClaseAPIRequest;
use App\Models\Clase;
use App\Repositories\ClaseRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use InfyOm\Generator\Utils\ResponseUtil;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class ClaseController
 * @package App\Http\Controllers\API
 */

class ClaseAPIController extends InfyOmBaseController
{
    /** @var  ClaseRepository */
    private $claseRepository;

    public function __construct(ClaseRepository $claseRepo)
    {
        $this->claseRepository = $claseRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/clases",
     *      summary="Get a listing of the Clases.",
     *      tags={"Clase"},
     *      description="Get all Clases",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Clase")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->claseRepository->pushCriteria(new RequestCriteria($request));
        $this->claseRepository->pushCriteria(new LimitOffsetCriteria($request));
        $clases = $this->claseRepository->all();

        return $this->sendResponse($clases->toArray(), 'Clases retrieved successfully');
    }

    /**
     * @param CreateClaseAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/clases",
     *      summary="Store a newly created Clase in storage",
     *      tags={"Clase"},
     *      description="Store Clase",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Clase that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Clase")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Clase"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateClaseAPIRequest $request)
    {
        $input = $request->all();

        $clases = $this->claseRepository->create($input);

        return $this->sendResponse($clases->toArray(), 'Clase saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/clases/{id}",
     *      summary="Display the specified Clase",
     *      tags={"Clase"},
     *      description="Get Clase",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Clase",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Clase"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Clase $clase */
        $clase = $this->claseRepository->find($id);

        if (empty($clase)) {
            return Response::json(ResponseUtil::makeError('Clase not found'), 404);
        }

        return $this->sendResponse($clase->toArray(), 'Clase retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateClaseAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/clases/{id}",
     *      summary="Update the specified Clase in storage",
     *      tags={"Clase"},
     *      description="Update Clase",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Clase",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Clase that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Clase")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Clase"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateClaseAPIRequest $request)
    {
        $input = $request->all();

        /** @var Clase $clase */
        $clase = $this->claseRepository->find($id);

        if (empty($clase)) {
            return Response::json(ResponseUtil::makeError('Clase not found'), 404);
        }

        $clase = $this->claseRepository->update($input, $id);

        return $this->sendResponse($clase->toArray(), 'Clase updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/clases/{id}",
     *      summary="Remove the specified Clase from storage",
     *      tags={"Clase"},
     *      description="Delete Clase",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Clase",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Clase $clase */
        $clase = $this->claseRepository->find($id);

        if (empty($clase)) {
            return Response::json(ResponseUtil::makeError('Clase not found'), 404);
        }

        $clase->delete();

        return $this->sendResponse($id, 'Clase deleted successfully');
    }
}

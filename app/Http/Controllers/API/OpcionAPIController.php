<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateOpcionAPIRequest;
use App\Http\Requests\API\UpdateOpcionAPIRequest;
use App\Models\Opcion;
use App\Repositories\OpcionRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use InfyOm\Generator\Utils\ResponseUtil;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class OpcionController
 * @package App\Http\Controllers\API
 */

class OpcionAPIController extends InfyOmBaseController
{
    /** @var  OpcionRepository */
    private $opcionRepository;

    public function __construct(OpcionRepository $opcionRepo)
    {
        $this->opcionRepository = $opcionRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/opcions",
     *      summary="Get a listing of the Opcions.",
     *      tags={"Opcion"},
     *      description="Get all Opcions",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Opcion")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->opcionRepository->pushCriteria(new RequestCriteria($request));
        $this->opcionRepository->pushCriteria(new LimitOffsetCriteria($request));
        $opcions = $this->opcionRepository->all();

        return $this->sendResponse($opcions->toArray(), 'Opcions retrieved successfully');
    }

    /**
     * @param CreateOpcionAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/opcions",
     *      summary="Store a newly created Opcion in storage",
     *      tags={"Opcion"},
     *      description="Store Opcion",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Opcion that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Opcion")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Opcion"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateOpcionAPIRequest $request)
    {
        $input = $request->all();

        $opcions = $this->opcionRepository->create($input);

        return $this->sendResponse($opcions->toArray(), 'Opcion saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/opcions/{id}",
     *      summary="Display the specified Opcion",
     *      tags={"Opcion"},
     *      description="Get Opcion",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Opcion",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Opcion"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Opcion $opcion */
        $opcion = $this->opcionRepository->find($id);

        if (empty($opcion)) {
            return Response::json(ResponseUtil::makeError('Opcion not found'), 404);
        }

        return $this->sendResponse($opcion->toArray(), 'Opcion retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateOpcionAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/opcions/{id}",
     *      summary="Update the specified Opcion in storage",
     *      tags={"Opcion"},
     *      description="Update Opcion",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Opcion",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Opcion that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Opcion")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Opcion"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateOpcionAPIRequest $request)
    {
        $input = $request->all();

        /** @var Opcion $opcion */
        $opcion = $this->opcionRepository->find($id);

        if (empty($opcion)) {
            return Response::json(ResponseUtil::makeError('Opcion not found'), 404);
        }

        $opcion = $this->opcionRepository->update($input, $id);

        return $this->sendResponse($opcion->toArray(), 'Opcion updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/opcions/{id}",
     *      summary="Remove the specified Opcion from storage",
     *      tags={"Opcion"},
     *      description="Delete Opcion",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Opcion",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Opcion $opcion */
        $opcion = $this->opcionRepository->find($id);

        if (empty($opcion)) {
            return Response::json(ResponseUtil::makeError('Opcion not found'), 404);
        }

        $opcion->delete();

        return $this->sendResponse($id, 'Opcion deleted successfully');
    }
}

<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateEvaluacionAPIRequest;
use App\Http\Requests\API\UpdateEvaluacionAPIRequest;
use App\Models\Evaluacion;
use App\Repositories\EvaluacionRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use InfyOm\Generator\Utils\ResponseUtil;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class EvaluacionController
 * @package App\Http\Controllers\API
 */

class EvaluacionAPIController extends InfyOmBaseController
{
    /** @var  EvaluacionRepository */
    private $evaluacionRepository;

    public function __construct(EvaluacionRepository $evaluacionRepo)
    {
        $this->evaluacionRepository = $evaluacionRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/evaluacions",
     *      summary="Get a listing of the Evaluacions.",
     *      tags={"Evaluacion"},
     *      description="Get all Evaluacions",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Evaluacion")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->evaluacionRepository->pushCriteria(new RequestCriteria($request));
        $this->evaluacionRepository->pushCriteria(new LimitOffsetCriteria($request));
        $evaluacions = $this->evaluacionRepository->all();

        return $this->sendResponse($evaluacions->toArray(), 'Evaluacions retrieved successfully');
    }

    /**
     * @param CreateEvaluacionAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/evaluacions",
     *      summary="Store a newly created Evaluacion in storage",
     *      tags={"Evaluacion"},
     *      description="Store Evaluacion",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Evaluacion that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Evaluacion")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Evaluacion"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateEvaluacionAPIRequest $request)
    {
        $input = $request->all();

        $evaluacions = $this->evaluacionRepository->create($input);

        return $this->sendResponse($evaluacions->toArray(), 'Evaluacion saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/evaluacions/{id}",
     *      summary="Display the specified Evaluacion",
     *      tags={"Evaluacion"},
     *      description="Get Evaluacion",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Evaluacion",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Evaluacion"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Evaluacion $evaluacion */
        $evaluacion = $this->evaluacionRepository->find($id);

        if (empty($evaluacion)) {
            return Response::json(ResponseUtil::makeError('Evaluacion not found'), 404);
        }

        return $this->sendResponse($evaluacion->toArray(), 'Evaluacion retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateEvaluacionAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/evaluacions/{id}",
     *      summary="Update the specified Evaluacion in storage",
     *      tags={"Evaluacion"},
     *      description="Update Evaluacion",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Evaluacion",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Evaluacion that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Evaluacion")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Evaluacion"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateEvaluacionAPIRequest $request)
    {
        $input = $request->all();

        /** @var Evaluacion $evaluacion */
        $evaluacion = $this->evaluacionRepository->find($id);

        if (empty($evaluacion)) {
            return Response::json(ResponseUtil::makeError('Evaluacion not found'), 404);
        }

        $evaluacion = $this->evaluacionRepository->update($input, $id);

        return $this->sendResponse($evaluacion->toArray(), 'Evaluacion updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/evaluacions/{id}",
     *      summary="Remove the specified Evaluacion from storage",
     *      tags={"Evaluacion"},
     *      description="Delete Evaluacion",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Evaluacion",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Evaluacion $evaluacion */
        $evaluacion = $this->evaluacionRepository->find($id);

        if (empty($evaluacion)) {
            return Response::json(ResponseUtil::makeError('Evaluacion not found'), 404);
        }

        $evaluacion->delete();

        return $this->sendResponse($id, 'Evaluacion deleted successfully');
    }
}

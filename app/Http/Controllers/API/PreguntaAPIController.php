<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePreguntaAPIRequest;
use App\Http\Requests\API\UpdatePreguntaAPIRequest;
use App\Models\Pregunta;
use App\Repositories\PreguntaRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use InfyOm\Generator\Utils\ResponseUtil;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class PreguntaController
 * @package App\Http\Controllers\API
 */

class PreguntaAPIController extends InfyOmBaseController
{
    /** @var  PreguntaRepository */
    private $preguntaRepository;

    public function __construct(PreguntaRepository $preguntaRepo)
    {
        $this->preguntaRepository = $preguntaRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/preguntas",
     *      summary="Get a listing of the Preguntas.",
     *      tags={"Pregunta"},
     *      description="Get all Preguntas",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Pregunta")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->preguntaRepository->pushCriteria(new RequestCriteria($request));
        $this->preguntaRepository->pushCriteria(new LimitOffsetCriteria($request));
        $preguntas = $this->preguntaRepository->all();

        return $this->sendResponse($preguntas->toArray(), 'Preguntas retrieved successfully');
    }

    /**
     * @param CreatePreguntaAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/preguntas",
     *      summary="Store a newly created Pregunta in storage",
     *      tags={"Pregunta"},
     *      description="Store Pregunta",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Pregunta that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Pregunta")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Pregunta"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreatePreguntaAPIRequest $request)
    {
        $input = $request->all();

        $preguntas = $this->preguntaRepository->create($input);

        return $this->sendResponse($preguntas->toArray(), 'Pregunta saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/preguntas/{id}",
     *      summary="Display the specified Pregunta",
     *      tags={"Pregunta"},
     *      description="Get Pregunta",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Pregunta",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Pregunta"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Pregunta $pregunta */
        $pregunta = $this->preguntaRepository->find($id);

        if (empty($pregunta)) {
            return Response::json(ResponseUtil::makeError('Pregunta not found'), 404);
        }

        return $this->sendResponse($pregunta->toArray(), 'Pregunta retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdatePreguntaAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/preguntas/{id}",
     *      summary="Update the specified Pregunta in storage",
     *      tags={"Pregunta"},
     *      description="Update Pregunta",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Pregunta",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Pregunta that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Pregunta")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Pregunta"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdatePreguntaAPIRequest $request)
    {
        $input = $request->all();

        /** @var Pregunta $pregunta */
        $pregunta = $this->preguntaRepository->find($id);

        if (empty($pregunta)) {
            return Response::json(ResponseUtil::makeError('Pregunta not found'), 404);
        }

        $pregunta = $this->preguntaRepository->update($input, $id);

        return $this->sendResponse($pregunta->toArray(), 'Pregunta updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/preguntas/{id}",
     *      summary="Remove the specified Pregunta from storage",
     *      tags={"Pregunta"},
     *      description="Delete Pregunta",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Pregunta",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Pregunta $pregunta */
        $pregunta = $this->preguntaRepository->find($id);

        if (empty($pregunta)) {
            return Response::json(ResponseUtil::makeError('Pregunta not found'), 404);
        }

        $pregunta->delete();

        return $this->sendResponse($id, 'Pregunta deleted successfully');
    }
}

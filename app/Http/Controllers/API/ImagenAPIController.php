<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateImagenAPIRequest;
use App\Http\Requests\API\UpdateImagenAPIRequest;
use App\Models\Imagen;
use App\Repositories\ImagenRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use InfyOm\Generator\Utils\ResponseUtil;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class ImagenController
 * @package App\Http\Controllers\API
 */

class ImagenAPIController extends InfyOmBaseController
{
    /** @var  ImagenRepository */
    private $imagenRepository;

    public function __construct(ImagenRepository $imagenRepo)
    {
        $this->imagenRepository = $imagenRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/imagens",
     *      summary="Get a listing of the Imagens.",
     *      tags={"Imagen"},
     *      description="Get all Imagens",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Imagen")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->imagenRepository->pushCriteria(new RequestCriteria($request));
        $this->imagenRepository->pushCriteria(new LimitOffsetCriteria($request));
        $imagens = $this->imagenRepository->all();

        return $this->sendResponse($imagens->toArray(), 'Imagens retrieved successfully');
    }

    /**
     * @param CreateImagenAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/imagens",
     *      summary="Store a newly created Imagen in storage",
     *      tags={"Imagen"},
     *      description="Store Imagen",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Imagen that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Imagen")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Imagen"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateImagenAPIRequest $request)
    {
        $input = $request->all();

        $imagens = $this->imagenRepository->create($input);

        return $this->sendResponse($imagens->toArray(), 'Imagen saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/imagens/{id}",
     *      summary="Display the specified Imagen",
     *      tags={"Imagen"},
     *      description="Get Imagen",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Imagen",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Imagen"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Imagen $imagen */
        $imagen = $this->imagenRepository->find($id);

        if (empty($imagen)) {
            return Response::json(ResponseUtil::makeError('Imagen not found'), 404);
        }

        return $this->sendResponse($imagen->toArray(), 'Imagen retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateImagenAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/imagens/{id}",
     *      summary="Update the specified Imagen in storage",
     *      tags={"Imagen"},
     *      description="Update Imagen",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Imagen",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Imagen that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Imagen")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Imagen"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateImagenAPIRequest $request)
    {
        $input = $request->all();

        /** @var Imagen $imagen */
        $imagen = $this->imagenRepository->find($id);

        if (empty($imagen)) {
            return Response::json(ResponseUtil::makeError('Imagen not found'), 404);
        }

        $imagen = $this->imagenRepository->update($input, $id);

        return $this->sendResponse($imagen->toArray(), 'Imagen updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/imagens/{id}",
     *      summary="Remove the specified Imagen from storage",
     *      tags={"Imagen"},
     *      description="Delete Imagen",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Imagen",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Imagen $imagen */
        $imagen = $this->imagenRepository->find($id);

        if (empty($imagen)) {
            return Response::json(ResponseUtil::makeError('Imagen not found'), 404);
        }

        $imagen->delete();

        return $this->sendResponse($id, 'Imagen deleted successfully');
    }
}

<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateTemaAPIRequest;
use App\Http\Requests\API\UpdateTemaAPIRequest;
use App\Models\Tema;
use App\Repositories\TemaRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use InfyOm\Generator\Utils\ResponseUtil;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class TemaController
 * @package App\Http\Controllers\API
 */

class TemaAPIController extends InfyOmBaseController
{
    /** @var  TemaRepository */
    private $temaRepository;

    public function __construct(TemaRepository $temaRepo)
    {
        $this->temaRepository = $temaRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/temas",
     *      summary="Get a listing of the Temas.",
     *      tags={"Tema"},
     *      description="Get all Temas",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Tema")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->temaRepository->pushCriteria(new RequestCriteria($request));
        $this->temaRepository->pushCriteria(new LimitOffsetCriteria($request));
        $temas = $this->temaRepository->all();

        return $this->sendResponse($temas->toArray(), 'Temas retrieved successfully');
    }

    /**
     * @param CreateTemaAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/temas",
     *      summary="Store a newly created Tema in storage",
     *      tags={"Tema"},
     *      description="Store Tema",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Tema that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Tema")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Tema"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateTemaAPIRequest $request)
    {
        $input = $request->all();

        $temas = $this->temaRepository->create($input);

        return $this->sendResponse($temas->toArray(), 'Tema saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/temas/{id}",
     *      summary="Display the specified Tema",
     *      tags={"Tema"},
     *      description="Get Tema",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Tema",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Tema"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Tema $tema */
        $tema = $this->temaRepository->find($id);

        if (empty($tema)) {
            return Response::json(ResponseUtil::makeError('Tema not found'), 404);
        }

        return $this->sendResponse($tema->toArray(), 'Tema retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateTemaAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/temas/{id}",
     *      summary="Update the specified Tema in storage",
     *      tags={"Tema"},
     *      description="Update Tema",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Tema",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Tema that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Tema")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Tema"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateTemaAPIRequest $request)
    {
        $input = $request->all();

        /** @var Tema $tema */
        $tema = $this->temaRepository->find($id);

        if (empty($tema)) {
            return Response::json(ResponseUtil::makeError('Tema not found'), 404);
        }

        $tema = $this->temaRepository->update($input, $id);

        return $this->sendResponse($tema->toArray(), 'Tema updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/temas/{id}",
     *      summary="Remove the specified Tema from storage",
     *      tags={"Tema"},
     *      description="Delete Tema",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Tema",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Tema $tema */
        $tema = $this->temaRepository->find($id);

        if (empty($tema)) {
            return Response::json(ResponseUtil::makeError('Tema not found'), 404);
        }

        $tema->delete();

        return $this->sendResponse($id, 'Tema deleted successfully');
    }
}

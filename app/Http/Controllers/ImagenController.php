<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateImagenRequest;
use App\Http\Requests\UpdateImagenRequest;
use App\Repositories\ImagenRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class ImagenController extends InfyOmBaseController
{
    /** @var  ImagenRepository */
    private $imagenRepository;

    public function __construct(ImagenRepository $imagenRepo)
    {
        $this->imagenRepository = $imagenRepo;
    }

    /**
     * Display a listing of the Imagen.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->imagenRepository->pushCriteria(new RequestCriteria($request));
        $imagens = $this->imagenRepository->all();

        return view('imagens.index')
            ->with('imagens', $imagens);
    }

    /**
     * Show the form for creating a new Imagen.
     *
     * @return Response
     */
    public function create()
    {
        return view('imagens.create');
    }

    /**
     * Store a newly created Imagen in storage.
     *
     * @param CreateImagenRequest $request
     *
     * @return Response
     */
    public function store(CreateImagenRequest $request)
    {
        $input = $request->all();

        $imagen = $this->imagenRepository->create($input);

        Flash::success('Imagen saved successfully.');

        return redirect(route('imagens.index'));
    }

    /**
     * Display the specified Imagen.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $imagen = $this->imagenRepository->findWithoutFail($id);

        if (empty($imagen)) {
            Flash::error('Imagen not found');

            return redirect(route('imagens.index'));
        }

        return view('imagens.show')->with('imagen', $imagen);
    }

    /**
     * Show the form for editing the specified Imagen.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $imagen = $this->imagenRepository->findWithoutFail($id);

        if (empty($imagen)) {
            Flash::error('Imagen not found');

            return redirect(route('imagens.index'));
        }

        return view('imagens.edit')->with('imagen', $imagen);
    }

    /**
     * Update the specified Imagen in storage.
     *
     * @param  int              $id
     * @param UpdateImagenRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateImagenRequest $request)
    {
        $imagen = $this->imagenRepository->findWithoutFail($id);

        if (empty($imagen)) {
            Flash::error('Imagen not found');

            return redirect(route('imagens.index'));
        }

        $imagen = $this->imagenRepository->update($request->all(), $id);

        Flash::success('Imagen updated successfully.');

        return redirect(route('imagens.index'));
    }

    /**
     * Remove the specified Imagen from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $imagen = $this->imagenRepository->findWithoutFail($id);

        if (empty($imagen)) {
            Flash::error('Imagen not found');

            return redirect(route('imagens.index'));
        }

        $this->imagenRepository->delete($id);

        Flash::success('Imagen deleted successfully.');

        return redirect(route('imagens.index'));
    }
}

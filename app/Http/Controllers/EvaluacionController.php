<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateEvaluacionRequest;
use App\Http\Requests\UpdateEvaluacionRequest;
use App\Repositories\EvaluacionRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use App\User;
use App\Models\Persona;
use App\Models\Clase;
use App\Models\Pregunta;
use App\Models\Opcion;
use App\Models\Evaluacion;

class EvaluacionController extends InfyOmBaseController
{
    /** @var  EvaluacionRepository */
    private $evaluacionRepository;

    public function __construct(EvaluacionRepository $evaluacionRepo)
    {
        $this->evaluacionRepository = $evaluacionRepo;
    }

    /**
     * Display a listing of the Evaluacion.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->evaluacionRepository->pushCriteria(new RequestCriteria($request));
        $evaluacions = $this->evaluacionRepository->all();

        return view('evaluacions.index')
            ->with('evaluacions', $evaluacions);
    }

    public function practica1(Request $request)
    {   
        $evaluacions = Evaluacion::where('id', '1')->get();
        $evaluacion = $evaluacions->first();
        $user_id = \Auth::user()->id;       
        $personas = Persona::where('user_id', $user_id)->get(); 
        $persona = $personas->first();
        $preguntas = Pregunta::where('evaluacion_id', '1')->get();
    
        return view('estudiante.modulos.1.practica1')
        ->with('persona', $persona)
        ->with('preguntas', $preguntas)
        ->with('evaluacion', $evaluacion);
    }

    /**
     * Show the form for creating a new Evaluacion.
     *
     * @return Response
     */
    public function create()
    {
        return view('evaluacions.create');
    }

    /**
     * Store a newly created Evaluacion in storage.
     *
     * @param CreateEvaluacionRequest $request
     *
     * @return Response
     */
    public function store(CreateEvaluacionRequest $request)
    {
        $input = $request->all();

        $evaluacion = $this->evaluacionRepository->create($input);

        Flash::success('Evaluacion saved successfully.');

        return redirect(route('evaluacions.index'));
    }

    /**
     * Display the specified Evaluacion.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $evaluacion = $this->evaluacionRepository->findWithoutFail($id);

        if (empty($evaluacion)) {
            Flash::error('Evaluacion not found');

            return redirect(route('evaluacions.index'));
        }

        return view('evaluacions.show')->with('evaluacion', $evaluacion);
    }

    /**
     * Show the form for editing the specified Evaluacion.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $evaluacion = $this->evaluacionRepository->findWithoutFail($id);

        if (empty($evaluacion)) {
            Flash::error('Evaluacion not found');

            return redirect(route('evaluacions.index'));
        }

        return view('evaluacions.edit')->with('evaluacion', $evaluacion);
    }

    /**
     * Update the specified Evaluacion in storage.
     *
     * @param  int              $id
     * @param UpdateEvaluacionRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateEvaluacionRequest $request)
    {
        $evaluacion = $this->evaluacionRepository->findWithoutFail($id);

        if (empty($evaluacion)) {
            Flash::error('Evaluacion not found');

            return redirect(route('evaluacions.index'));
        }

        $evaluacion = $this->evaluacionRepository->update($request->all(), $id);

        Flash::success('Evaluacion updated successfully.');

        return redirect(route('evaluacions.index'));
    }

    /**
     * Remove the specified Evaluacion from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $evaluacion = $this->evaluacionRepository->findWithoutFail($id);

        if (empty($evaluacion)) {
            Flash::error('Evaluacion not found');

            return redirect(route('evaluacions.index'));
        }

        $this->evaluacionRepository->delete($id);

        Flash::success('Evaluacion deleted successfully.');

        return redirect(route('evaluacions.index'));
    }
}

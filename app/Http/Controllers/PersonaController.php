<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreatePersonaRequest;
use App\Http\Requests\UpdatePersonaRequest;
use App\Repositories\PersonaRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use App\User;
use App\Models\Persona;
use App\Models\Modulo;

class PersonaController extends InfyOmBaseController
{
    /** @var  PersonaRepository */
    private $personaRepository;

    public function __construct(PersonaRepository $personaRepo)
    {
        $this->personaRepository = $personaRepo;
    }

    /**
     * Display a listing of the Persona.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->personaRepository->pushCriteria(new RequestCriteria($request));
        $personas = $this->personaRepository->all();

        return view('personas.index')
            ->with('personas', $personas);
    }

    public function inicio(Request $request)
    {
        $user_id = \Auth::user()->id;       
        $personas = Persona::where('user_id', $user_id)->get(); 
        $persona = $personas->first();
        $modulos = Modulo::orderBy('id','ASC')->get();
        return view('estudiante.index')
        ->with('persona', $persona)
        ->with('modulos', $modulos);
    }

    /**
     * Show the form for creating a new Persona.
     *
     * @return Response
     */
    public function registrar()
    {
        return view('public.registrar');
    }

    public function create()
    {
        return view('personas.create');
    }

    /**
     * Store a newly created Persona in storage.
     *
     * @param CreatePersonaRequest $request
     *
     * @return Response
     */
    public function store(CreatePersonaRequest $request)
    {
        $input = $request->all();
        $emails = User::where('email', $request->email)->get();
        if (count($emails)>0) {
            Flash::error('Lo sentimos, ya existe un usuario con ese nombre de Email.');
            return redirect(route('public.registrar'));    
        }

        $cedulas = Persona::where('cedula', $request->cedula)->get();
        if (count($cedulas)>0) {
            Flash::error('Lo sentimos, ya existe un usuario con esa Cedula.');
            return redirect(route('public.registrar'));    
        }

        $user = new User();
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->tipo = "estudiante";
        $user->save();
        $user_id = $user->id;

        $persona = new Persona();
        $persona->nombre = $request->nombre;
        $persona->apellido = $request->apellido;
        $persona->cedula = $request->cedula;
        $persona->fech_nac = $request->fech_nac;
        $persona->user_id = $user_id;
        $file = $request->avatar;
        $nombre = $request->nombre.'_'.$request->cedula.'_.'.$file->getClientOriginalExtension();  
        $ruta = public_path().'/img/avatar/';
        $file->move($ruta, $nombre);
        $persona->avatar = '/img/avatar/'.$nombre;
        $persona->save();

        Flash::success('Registrado Correctamente. Inicie Sesion');

        return redirect(route('auth.login'));
    }

    /**
     * Display the specified Persona.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $persona = $this->personaRepository->findWithoutFail($id);

        if (empty($persona)) {
            Flash::error('Persona not found');

            return redirect(route('personas.index'));
        }

        return view('personas.show')->with('persona', $persona);
    }

    /**
     * Show the form for editing the specified Persona.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $persona = $this->personaRepository->findWithoutFail($id);

        if (empty($persona)) {
            Flash::error('Persona not found');

            return redirect(route('personas.index'));
        }

        return view('personas.edit')->with('persona', $persona);
    }

    /**
     * Update the specified Persona in storage.
     *
     * @param  int              $id
     * @param UpdatePersonaRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePersonaRequest $request)
    {
        $persona = $this->personaRepository->findWithoutFail($id);

        if (empty($persona)) {
            Flash::error('Persona not found');

            return redirect(route('personas.index'));
        }

        $persona = $this->personaRepository->update($request->all(), $id);

        Flash::success('Persona updated successfully.');

        return redirect(route('personas.index'));
    }

    /**
     * Remove the specified Persona from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $persona = $this->personaRepository->findWithoutFail($id);

        if (empty($persona)) {
            Flash::error('Persona not found');

            return redirect(route('personas.index'));
        }

        $this->personaRepository->delete($id);

        Flash::success('Persona deleted successfully.');

        return redirect(route('personas.index'));
    }
}

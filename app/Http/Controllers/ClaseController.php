<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateClaseRequest;
use App\Http\Requests\UpdateClaseRequest;
use App\Repositories\ClaseRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use App\User;
use App\Models\Persona;
use App\Models\Clase;

class ClaseController extends InfyOmBaseController
{
    /** @var  ClaseRepository */
    private $claseRepository;

    public function __construct(ClaseRepository $claseRepo)
    {
        $this->claseRepository = $claseRepo;
    }

    /**
     * Display a listing of the Clase.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->claseRepository->pushCriteria(new RequestCriteria($request));
        $clases = $this->claseRepository->all();

        return view('clases.index')
            ->with('clases', $clases);
    }

     public function clase1(Request $request)
    {
        $user_id = \Auth::user()->id;       
        $personas = Persona::where('user_id', $user_id)->get(); 
        $persona = $personas->first();
        return view('estudiante.modulos.1.clase1')
        ->with('persona', $persona);
    }

    /**
     * Show the form for creating a new Clase.
     *
     * @return Response
     */
    public function create()
    {
        return view('clases.create');
    }

    /**
     * Store a newly created Clase in storage.
     *
     * @param CreateClaseRequest $request
     *
     * @return Response
     */
    public function store(CreateClaseRequest $request)
    {
        $input = $request->all();

        $clase = $this->claseRepository->create($input);

        Flash::success('Clase saved successfully.');

        return redirect(route('clases.index'));
    }

    /**
     * Display the specified Clase.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $clase = $this->claseRepository->findWithoutFail($id);

        if (empty($clase)) {
            Flash::error('Clase not found');

            return redirect(route('clases.index'));
        }

        return view('clases.show')->with('clase', $clase);
    }

    /**
     * Show the form for editing the specified Clase.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $clase = $this->claseRepository->findWithoutFail($id);

        if (empty($clase)) {
            Flash::error('Clase not found');

            return redirect(route('clases.index'));
        }

        return view('clases.edit')->with('clase', $clase);
    }

    /**
     * Update the specified Clase in storage.
     *
     * @param  int              $id
     * @param UpdateClaseRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateClaseRequest $request)
    {
        $clase = $this->claseRepository->findWithoutFail($id);

        if (empty($clase)) {
            Flash::error('Clase not found');

            return redirect(route('clases.index'));
        }

        $clase = $this->claseRepository->update($request->all(), $id);

        Flash::success('Clase updated successfully.');

        return redirect(route('clases.index'));
    }

    /**
     * Remove the specified Clase from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $clase = $this->claseRepository->findWithoutFail($id);

        if (empty($clase)) {
            Flash::error('Clase not found');

            return redirect(route('clases.index'));
        }

        $this->claseRepository->delete($id);

        Flash::success('Clase deleted successfully.');

        return redirect(route('clases.index'));
    }
}

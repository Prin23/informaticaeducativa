<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateOpcionRequest;
use App\Http\Requests\UpdateOpcionRequest;
use App\Repositories\OpcionRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class OpcionController extends InfyOmBaseController
{
    /** @var  OpcionRepository */
    private $opcionRepository;

    public function __construct(OpcionRepository $opcionRepo)
    {
        $this->opcionRepository = $opcionRepo;
    }

    /**
     * Display a listing of the Opcion.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->opcionRepository->pushCriteria(new RequestCriteria($request));
        $opcions = $this->opcionRepository->all();

        return view('opcions.index')
            ->with('opcions', $opcions);
    }

    /**
     * Show the form for creating a new Opcion.
     *
     * @return Response
     */
    public function create()
    {
        return view('opcions.create');
    }

    /**
     * Store a newly created Opcion in storage.
     *
     * @param CreateOpcionRequest $request
     *
     * @return Response
     */
    public function store(CreateOpcionRequest $request)
    {
        $input = $request->all();

        $opcion = $this->opcionRepository->create($input);

        Flash::success('Opcion saved successfully.');

        return redirect(route('opcions.index'));
    }

    /**
     * Display the specified Opcion.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $opcion = $this->opcionRepository->findWithoutFail($id);

        if (empty($opcion)) {
            Flash::error('Opcion not found');

            return redirect(route('opcions.index'));
        }

        return view('opcions.show')->with('opcion', $opcion);
    }

    /**
     * Show the form for editing the specified Opcion.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $opcion = $this->opcionRepository->findWithoutFail($id);

        if (empty($opcion)) {
            Flash::error('Opcion not found');

            return redirect(route('opcions.index'));
        }

        return view('opcions.edit')->with('opcion', $opcion);
    }

    /**
     * Update the specified Opcion in storage.
     *
     * @param  int              $id
     * @param UpdateOpcionRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateOpcionRequest $request)
    {
        $opcion = $this->opcionRepository->findWithoutFail($id);

        if (empty($opcion)) {
            Flash::error('Opcion not found');

            return redirect(route('opcions.index'));
        }

        $opcion = $this->opcionRepository->update($request->all(), $id);

        Flash::success('Opcion updated successfully.');

        return redirect(route('opcions.index'));
    }

    /**
     * Remove the specified Opcion from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $opcion = $this->opcionRepository->findWithoutFail($id);

        if (empty($opcion)) {
            Flash::error('Opcion not found');

            return redirect(route('opcions.index'));
        }

        $this->opcionRepository->delete($id);

        Flash::success('Opcion deleted successfully.');

        return redirect(route('opcions.index'));
    }
}

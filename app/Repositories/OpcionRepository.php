<?php

namespace App\Repositories;

use App\Models\Opcion;
use InfyOm\Generator\Common\BaseRepository;

class OpcionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'opcion',
        'pregunta_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Opcion::class;
    }
}

<?php

namespace App\Repositories;

use App\Models\Seccion;
use InfyOm\Generator\Common\BaseRepository;

class SeccionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'titulo'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Seccion::class;
    }
}

<?php

namespace App\Repositories;

use App\Models\Evaluacion;
use InfyOm\Generator\Common\BaseRepository;

class EvaluacionRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'titulo',
        'tipo',
        'modulo_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Evaluacion::class;
    }
}

<?php

namespace App\Repositories;

use App\Models\Clase;
use InfyOm\Generator\Common\BaseRepository;

class ClaseRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'titulo',
        'contenido',
        'modulo_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Clase::class;
    }
}

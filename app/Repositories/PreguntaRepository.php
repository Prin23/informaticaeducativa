<?php

namespace App\Repositories;

use App\Models\Pregunta;
use InfyOm\Generator\Common\BaseRepository;

class PreguntaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'pregunta',
        'evaluacion_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Pregunta::class;
    }
}

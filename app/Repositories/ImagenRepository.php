<?php

namespace App\Repositories;

use App\Models\Imagen;
use InfyOm\Generator\Common\BaseRepository;

class ImagenRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'imagen',
        'clase_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Imagen::class;
    }
}

<?php

namespace App\Repositories;

use App\Models\Modulo;
use InfyOm\Generator\Common\BaseRepository;

class ModuloRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'titulo',
        'descripcion'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Modulo::class;
    }
}

<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Tema",
 *      required={"titulo", "descripcion", "miniatura"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="titulo",
 *          description="titulo",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="descripcion",
 *          description="descripcion",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="miniatura",
 *          description="miniatura",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Tema extends Model
{
    use SoftDeletes;

    public $table = 'temas';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'titulo',
        'slug',
        'descripcion',
        'miniatura',
        'estado',
        'seccion_id',
        'user_id',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'titulo' => 'string',
        'slug' => 'string',
        'descripcion' => 'string',
        'miniatura' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'titulo' => 'required',
        'descripcion' => 'required',
        'miniatura' => 'required'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function seccion()
    {
        return $this->belongsTo('App\Models\Seccion');
    }

    public function posts()
    {
        return $this->hasMany('App\Models\Post');
    }
}

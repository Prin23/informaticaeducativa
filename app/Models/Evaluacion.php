<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Evaluacion",
 *      required={"titulo", "tipo", "modulo_id"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="titulo",
 *          description="titulo",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="modulo_id",
 *          description="modulo_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Evaluacion extends Model
{
    use SoftDeletes;

    public $table = 'evaluacions';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'titulo',
        'tipo',
        'modulo_id'
    ];

     public function modulo()
    {
        $this->belongsTo('App\Models\Modulo');
    }

    public function preguntas()
    {
        return $this->hasMany('App\Models\Pregunta');
    }

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'titulo' => 'string',
        'modulo_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'titulo' => 'required',
        'tipo' => 'required',
        'modulo_id' => 'required'
    ];
}

<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Pregunta",
 *      required={"pregunta", "evaluacion_id"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="pregunta",
 *          description="pregunta",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="evaluacion_id",
 *          description="evaluacion_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Pregunta extends Model
{
    use SoftDeletes;

    public $table = 'preguntas';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'pregunta',
        'evaluacion_id'
    ];

    public function evaluacion(){
        return $this->belongsTo('App\Models\Evaluacion');
    }

    public function opciones()
    {
        return $this->hasMany('App\Models\Opcion');
    }
    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'pregunta' => 'string',
        'evaluacion_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'pregunta' => 'required',
        'evaluacion_id' => 'required'
    ];
}

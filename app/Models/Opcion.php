<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Opcion",
 *      required={"opcion", "pregunta_id"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="opcion",
 *          description="opcion",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="pregunta_id",
 *          description="pregunta_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Opcion extends Model
{
    use SoftDeletes;

    public $table = 'opcions';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'opcion',
        'tipo',
        'pregunta_id'
    ];

    public function pregunta()
    {
        return $this->belongsTo('App\Models\Pregunta');
    }

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'opcion' => 'string',
        'pregunta_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'opcion' => 'required',
        'pregunta_id' => 'required'
    ];
}

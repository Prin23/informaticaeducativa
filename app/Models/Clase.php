<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Clase",
 *      required={"titulo", "modulo_id"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="titulo",
 *          description="titulo",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="contenido",
 *          description="contenido",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="modulo_id",
 *          description="modulo_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Clase extends Model
{
    use SoftDeletes;

    public $table = 'clases';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'titulo',
        'contenido',
        'modulo_id'
    ];

    public function modulo()
    {
        return $this->belongsTo('App\Models\Modulo');
    }

    public function imagenes()
    {
        return $this->hasMany('App\Models\Imagen');
    }

    public function videos()
    {
        return $this->hasMany('App\Models\Videos');
    }

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'titulo' => 'string',
        'contenido' => 'string',
        'modulo_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'titulo' => 'required',
        'contenido' => 'required',
        'modulo_id' => 'required'
    ];
}
